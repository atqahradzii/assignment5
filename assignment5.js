function validateForm(){

var name = document.forms["myForm"]["name"].value;
var length = document.forms["myForm"]["length"].value;
var width = document.forms["myForm"]["width"].value;
var height = document.forms["myForm"]["height"].value;
var weight= (length*width*height)/5000;
var mode = document.forms["myForm"]["mode"].value;
var type = document.forms["myForm"]["type"].value;

alert("Parcel Volumetric and Cost Calculator"+"\nCustomer name: "+name+ "\nLength: "+length+" cm\nWidth: "
	+width+" cm\nHeight: "+height+" cm\nWeight: "+weight+" kg\nMode: "+mode+"\nType: "+type+"\nDelivery cost: RM "+calculateCost(mode, type, weight));
}

function resetFunction (){
	var reset= document.forms["myForm"]["reset"].value;
	alert("The inputs will be reset");
}

function upperCase() {

  var x = document.getElementById("name");
  x.value = x.value.toUpperCase();

}

function calculateCost(mode, type, weight) {

	if(mode=="Surface")
	{
		if (type=="Domestic") 
		{
			if (weight<2) 
			{
				var cost=7;
			}
			else if (weight>=2)
			{
				weight=weight-2;
				var cost=7+(weight*1.5);
			}
		}

		else if (type=="International") 
		{
			if (weight<2) 
			{
				var cost=20;
			}
			else if (weight>=2)
			{
				weight=weight-2;
				var cost=20+(weight*3);
			}
		}
	}

	else if(mode=="Air")
	{
		if (type=="Domestic") 
		{
			if (weight<2) 
			{
				var cost=10;
			}
			else if (weight>=2)
			{
				weight=weight-2;
				var cost=10+(weight*3);
			}
		}

		else if (type=="International") 
		{
			if (weight<2) 
			{
				var cost=50;
			}
			else if (weight>=2)
			{
				weight=weight-2;
				var cost=50+(weight*5);
			}
		}
	}

	return cost;
}

